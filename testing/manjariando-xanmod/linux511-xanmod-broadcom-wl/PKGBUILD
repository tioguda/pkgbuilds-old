# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Based on the file created for Arch Linux by: Frank Vanderham

_linuxprefix=linux511-xanmod
_extramodules=extramodules-5.11-xanmod-MANJARO
pkgname=$_linuxprefix-broadcom-wl
_pkgname=broadcom-wl
_pkgver=6.30.223.271
pkgver=6.30.223.271_5.11.11.xanmod1_1.1
pkgrel=1
pkgdesc='Broadcom 802.11 Linux STA wireless driver BCM43142.'
url='https://bbs.archlinux.org/viewtopic.php?id=145884'
arch=('x86_64')
license=('custom')
depends=("$_linuxprefix")
makedepends=("broadcom-wl-dkms>=$_pkgver"
             'dkms'
             "$_linuxprefix" "$_linuxprefix-headers")
groups=("$_linuxprefix-extramodules")
provides=("$_pkgname=$_pkgver")
install=$_pkgname.install
backup=('etc/modprobe.d/$_linuxprefix-broadcom-wl.conf')
source=(broadcom-wl-dkms.conf)
sha256sums=('b97bc588420d1542f73279e71975ccb5d81d75e534e7b5717e01d6e6adf6a283')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    _kernel=${_ver}
    printf '%s' "${_pkgver}_${_kernel/-/_}"
}

prepare() {
  # dkms need modification to be run as user
  cp -r /var/lib/dkms .
  echo "dkms_tree='$srcdir/dkms'" > dkms.conf
}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  # build host modules
  msg2 'Build module'

  CFLAGS="${CFLAGS} -Wno-error"
  CXXFLAGS="${CXXFLAGS}  -Wno-error"

  msg2 "$CFLAGS, ${CXXFLAGS}"

  dkms --dkmsframework dkms.conf build "broadcom-wl/$_pkgver" -k "$_kernver"
}

package(){
  depends=("$_linuxprefix")

  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  install -dm755 "$pkgdir/usr/lib/modules/$_extramodules"
  cd "dkms/broadcom-wl/$_pkgver/$_kernver/$CARCH/module"
  install -m644 * "$pkgdir/usr/lib/modules/$_extramodules"
  find "$pkgdir" -name '*.ko' -exec gzip -9 {} +
  sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='$_extramodules'/" "$startdir/$_pkgname.install"
  install -D -m 644 "${srcdir}"/broadcom-wl-dkms.conf "${pkgdir}/etc/modprobe.d/${_linuxprefix}-broadcom-wl.conf"
}
