# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=portugol-studio
pkgver=2.7.4
pkgrel=5
pkgdesc="Ambiente de Desenvolvimento para a linguagem Portugol"
arch=('x86_64')
url="https://univali-lite.github.io/Portugol-Studio"
license=('GPL-3.0')
makedepends=('imagemagick')
conflicts=("${pkgname}-bin")
source=("https://github.com/UNIVALI-LITE/Portugol-Studio/releases/download/v${pkgver}/portugol-studio-${pkgver}-linux-x64-standalone.zip"
        "LICENSE::https://raw.githubusercontent.com/UNIVALI-LITE/Portugol-Studio/master/LICENSE.md"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml")
sha256sums=('989e2ea47116880748f0200434bd40f9ebb7b18a63311a76a782a39822a92296'
            'da7eabb7bafdf7d3ae5e9f223aa5bdc1eece45ac569dc21b3b037520b4464768'
            'cf6bb6e6c536667baafba980e5c0718661940a12c3c179973a5bcaac264b5ecf')

_portugol_desktop="[Desktop Entry]
Name=Portugol Studio
Comment=Didactic Programming Environment for the Portugol Language
Comment[pt_BR]=Ambiente de Desenvolvimento para a linguagem Portugol
Exec=portugol-studio %F
Icon=portugol-studio
Type=Application
Terminal=false
Categories=Application;Development;Java;IDE;Portugol
MimeType=application/x-portugol"

build() {
    cd "${srcdir}"
    echo -e "$_portugol_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    cd "${srcdir}"

    # Create directories
    mkdir -p "${pkgdir}/usr/share/portugol-studio"
    mkdir -p "${pkgdir}/usr/share/mime/packages"
    mkdir -p "${pkgdir}/usr/bin"

    # Copy portugol-studio to /usr/share
    cp -R portugol-studio "${pkgdir}/usr/share"

    # Symlink
    ln -s /usr/share/portugol-studio/executar-studio-linux.sh "${pkgdir}/usr/bin/portugol-studio"
    ln -s /usr/share/portugol-studio/executar-console-linux.sh "${pkgdir}/usr/bin/portugol-console"

    # Mime
    cp arquivos-auxiliares/application-x-portugol.xml "${pkgdir}/usr/share/mime/packages/application-x-portugol.xml"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

    # Fix and install desktop icons
    for size in 22 24 32 48 64 128 256; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/usr/share/portugol-studio/aplicacao/icones/linux/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
        convert "${pkgdir}/usr/share/portugol-studio/aplicacao/icones/linux/arquivo-por.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/application-x-portugol.png"
    done

    rm -rf "${pkgdir}/usr/share/portugol-studio/aplicacao/icones/windows"

} 
