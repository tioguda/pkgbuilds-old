# Maintainer: lsf
# Contributor: Adam Hose <adis@blad.is>

pyver=3.9
_pkgname=opensnitch
pkgname=${_pkgname}-dev
pkgver=1.3.6.897
pkgrel=1
arch=('i686' 'x86_64')
license=('GPL3')
url='https://github.com/evilsocket/opensnitch'
pkgdesc='A GNU/Linux port of the Little Snitch application firewall.'
depends=('libnetfilter_queue' 'libpcap' 'python-grpcio' 'python-protobuf'
         'python-pyinotify' 'python-slugify' 'python-pyqt5')
optdepends=('logrotate: for logfile rotation support')
makedepends=('git' 'go' 'python-setuptools' 'python-grpcio-tools' 'imagemagick' "python>=${pyver}" 'qt5-tools')
conflicts=('opensnitch' 'opensnitch-git')
provides=('opensnitch' 'opensnitch-ui'  'opensnitch-git')
backup=('etc/opensnitchd/default-config.json')
install=${_pkgname}.install

source=("git://github.com/evilsocket/opensnitch.git"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "protoc-gen-go.patch::https://github.com/evilsocket/opensnitch/pull/381/commits/1f26f66e8abdfe7c5a15a99265af3d04dfdec54c.patch")
sha256sums=('SKIP'
            '06efbc258e4c77f9ac5411708fd3c4806b53fe41c59580f899892a97474499db'
            '4a25ddae4278a3f512857826fc854c5678655bd2061b3b654c03f3d27450924c')

pkgver() {
    cd "${srcdir}/${_pkgname}"
    _pkgver=$(git describe --long | sed 's/^v//;s/\([^-]*-\)g/r\1/;s/-/./g')
    printf '%s' "$( cut -f1-3 -d'.' <<< ${_pkgver} ).$(git rev-list --count HEAD)"
}

prepare() {
    export GOPATH="$srcdir/gopath"
    go clean -modcache

    cd "$srcdir/${_pkgname}"
    sed -i 's|local/bin|bin|g' "daemon/${_pkgname}d.service"
    
    patch -p1 -i ${srcdir}/protoc-gen-go.patch
}

build() {
    cd "$srcdir/${_pkgname}"

    pushd proto
    export GOPATH="$srcdir/gopath"
    export CGO_CPPFLAGS="${CPPFLAGS}"
    export CGO_CFLAGS="${CFLAGS}"
    export CGO_CXXFLAGS="${CXXFLAGS}"
    export CGO_LDFLAGS="${LDFLAGS}"
    export GOFLAGS="-buildmode=pie -trimpath -ldflags=-linkmode=external -mod=mod"
    export PATH=${PATH}:${GOPATH}/bin
    go get github.com/golang/protobuf/protoc-gen-go
    make
    popd

    pushd daemon
    make
    popd

    # Clean mod cache for makepkg -C
    go clean -modcache

    pushd ui
    pyrcc5 -o opensnitch/resources_rc.py opensnitch/res/resources.qrc
    python setup.py build
    popd
}

package() {
    cd "$srcdir/${_pkgname}"
    pushd ui
    export PYTHONHASHSEED=0
    python setup.py install --root="$pkgdir/" --optimize=1 --skip-build
    popd

    install -Dm755 "daemon/${_pkgname}d" -t "$pkgdir/usr/bin"
    install -Dm644 "daemon/${_pkgname}d.service" -t \
        "$pkgdir/usr/lib/systemd/system"
    install -dm755 "$pkgdir/etc/${_pkgname}d/rules"
    install -Dm644 daemon/default-config.json -t "$pkgdir/etc/${_pkgname}d"
    install -Dm644 daemon/system-fw.json -t "$pkgdir/etc/${_pkgname}d"
    install -Dm644 "debian/${_pkgname}.logrotate" \
        "$pkgdir/etc/logrotate.d/${_pkgname}"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    mv "${pkgdir}/usr/share/applications/opensnitch_ui.desktop" "${pkgdir}/usr/share/applications/com.opensnitch_ui.desktop"
    sed -i "s:Icon=.*:Icon=${pkgname}:" "${pkgdir}/usr/share/applications/com.opensnitch_ui.desktop"
    sed -i "s:Icon=.*:Icon=${pkgname}:" "${pkgdir}/usr/share/kservices5/kcm_opensnitch.desktop"

    # i18n
    for lang in de_DE es_ES eu_ES pt_BR; do
        lrelease "ui/i18n/locales/${lang}/opensnitch-${lang}.ts" -qm "ui/i18n/locales/${lang}/opensnitch-${lang}.qm"
        mkdir -p "${pkgdir}/usr/lib/python3.9/site-packages/opensnitch/i18n/${lang}/"
        cp "ui/i18n/locales/${lang}/opensnitch-${lang}.qm" "${pkgdir}/usr/lib/python${pyver}/site-packages/opensnitch/i18n/${lang}/"
    done

    # Fix and install desktop icons
    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/usr/lib/python3.9/site-packages/opensnitch/res/icon.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
