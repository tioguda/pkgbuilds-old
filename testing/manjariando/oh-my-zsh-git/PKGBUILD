# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor:  Marcin (CTRL) Wieczorek <marcin@marcin.co>
# Contributor: Simon (vodik) Gomizelj <simongmzlj@gmail.com>
# Contributor: Eduardo Leggiero <https://www.leggiero.uk/>
# Contributor: jyantis <yantis@yantis.net>
# Contributor: jcsiv <siviter dot jamie at gmx dot co dot uk>
# Contributor: ThinCarrotShrimp <christoph.r.martin+arch at gmail dot com>

_pkgname=oh-my-zsh
pkgname=${_pkgname}-git
pkgver=r5986
zshver=5.8
pkgrel=1
pkgdesc="A community-driven framework for managing your zsh configuration. Includes 180+ optional plugins and over 120 themes to spice up your morning, and an auto-update tool so that makes it easy to keep up with the latest updates from the community"
arch=('any')
url='https://ohmyz.sh'
license=('MIT')
makedepends=('git' "zsh=${zshver}")
provides=("${pkgname%%-git}")
conflicts=("${pkgname%%-git}")
optdepends=('git: most themes use git (highly recommended but still optional)'
            'ruby: for some plugin functionality'
            'python: for some plugin functionality'
            'oh-my-zsh-powerline-theme: great theme'
            'bullet-train-oh-my-zsh-theme: better powerline theme'
            'oh-my-zsh-autosuggestions: Fish-like autosuggestions for oh-my-zsh'
            'zsh-auto-notify: plugin that sends out a notification when a long running task has completed')
install=${pkgname%%-git}.install
source=("${pkgname%%-git}::git+git://github.com/robbyrussell/oh-my-zsh.git"
        "https://metainfo.manjariando.com.br/${_pkgname}/com.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${_pkgname}/${_pkgname}"-{48,64,128}.png
        '0001-zshrc.patch')
sha256sums=('SKIP'
            '00e4b6e2b3fb96328b6fc46040e0d490092242cbfc1edd1194b1126a37df964c'
            '4e6e7f86ea3e94b077332595246f8d415bab49bd17fab9b56892e194fc87efd0'
            'c8ffb66d3906ca92e060f5f12a0b934221711d3a220d0d2d1a5ea2f67254b9d1'
            '2c4e970178b365d63c75020b15adb5715be178f8da2f66e9c7edb4804707ad49'
            '9b77769319944f394a36f07b9abb296d24fe643c03b8eead74e10b7da52002b1')
# conflicts=('grml-zsh-config'
#            'grml-zsh-config-git')

pkgver() {
    cd ${pkgname%%-git}
    printf "r%s" "$(git rev-list --count HEAD)"
}

prepare() {
    cd "${srcdir}/${pkgname%%-git}"
    cp "templates/zshrc.zsh-template" "zshrc"
    patch -p1 < "${srcdir}/0001-zshrc.patch"
}

_zsh_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_zsh_desktop" | tee com.${_pkgname//-/_}.desktop
}

package() {
    depends=("zsh")

    cd "${srcdir}/${pkgname%%-git}"

    mkdir -p "${pkgdir}/usr/share/oh-my-zsh"
    install -D -m644 "LICENSE.txt" "${pkgdir}/usr/share/licenses/${pkgname%%-git}/LICENSE"
    cp -r * "${pkgdir}/usr/share/oh-my-zsh/"

    # Appstream
    install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${_pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${_pkgname//-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${_pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
    done

    # fix .install file
    sed -i "s:ZSH=.*:ZSH=${zshver}:" "${startdir}/${pkgname%%-git}.install"
}
