# Maintainer: Fabian Maurer <dark.shadow4@web.de>
# Contributor: Rafael Fontenelle <rafaelff@gnome.org>

_name=ldb
pkgname=lib32-$_name
pkgver=2.2.0
pkgrel=3
pkgdesc="A schema-less, ldap like, API and database"
arch=('x86_64')
url="https://ldb.samba.org"
license=('GPL3')
depends=('lib32-talloc' 'lib32-tevent' 'lib32-tdb' 'lib32-popt' 'lib32-libbsd' "ldb>=${pkgver}" 'python')
makedepends=('lib32-cmocka' 'lib32-gcc-libs')
source=(https://samba.org/ftp/${_name}/${_name}-${pkgver}.tar{.gz,.asc})
validpgpkeys=('9147A339719518EE9011BCB54793916113084025') # Samba Library Distribution Key <samba-bugs@samba.org>
sha512sums=('3814078a51d2110eeda291ac859c0027df88475812bd6b5f2ce8f8f50aba9c84faced97c37aa1f45e406783a8df97c60ae778df897bb0e89ba3ac8568acced69'
            'SKIP')

build() {
    cd ${_name}-${pkgver}

    export CC='gcc -m32'
    export CXX='g++ -m32'
    export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'

    ./configure \
        --prefix=/usr \
        --disable-rpath \
        --disable-rpath-install \
        --bundled-libraries=NONE \
        --builtin-libraries=replace \
        --libdir=/usr/lib32 \
        --with-modulesdir=/usr/lib32/ldb/modules \
        --with-privatelibdir=/usr/lib32/ldb \
        --disable-python
    
    make
}

package() {
    cd "${_name}-${pkgver}"
    make DESTDIR="${pkgdir}" install
    rm -rf "${pkgdir}"/usr/{bin,include,share}
}
