# Maintainer: tioguda <guda.flavio@gmail.com>

_pkgbase=langpack
pkgbase=libreoffice-dev-${_pkgbase}
_pkgnamefmt=LibreOffice
_LOver=7.1.2.2
pkgver=$(cut -f1-4 -d'.' <<< ${_LOver})
_basever=$( cut -f1-2 -d'.' <<< ${_LOver})
pkgrel=1
arch=('x86_64')
license=('LGPL3')
url="https://www.libreoffice.org"
pkgdesc="Language pack for LibreOffice development branch"

_languages=(
  'ar     "Arabic" "ar"'
  'ast    "Asturian" "ast"'
  'bg     "Bulgarian" "bg"'
  'bs     "Bosnian" "bs"'
  'ca     "Catalan" "ca"'
  'cs     "Czech" "cs"'
  'da     "Danish" "da"'
  'de     "German" "de"'
  'el     "Greek" "el"'
  'en-GB  "English (British)" "en-gb"'
  'en-ZA  "English (South African)" "en-za"'
  'eo     "Esperanto" "eo"'
  'es     "Spanish" "es"'
  'et     "Estonian" "et"'
  'eu     "Basque" "eu"'
  'fi     "Finnish" "fi"'
  'fr     "French" "fr"'
  'gl     "Galician" "gl"'
  'he     "Hebrew" "he"'
  'hr     "Croatian" "hr"'
  'hu     "Hungarian" "hu"'
  'id     "Indonesian" "id"'
  'is     "Icelandic" "is"'
  'it     "Italian" "it"'
  'ja     "Japanese" "ja"'
  'km     "Khmer" "km"'
  'ko     "Korean" "ko"'
  'mk     "Macedonian" "mk"'
  'nl     "Dutch" "nl"'
  'pl     "Polish" "pl"'
  'pt-BR  "Portuguese (Brazilian)" "pt-br"'
  'pt     "Portuguese (Portugal)" "pt"'
  'ro     "Romanian" "ro"'
  'ru     "Russian" "ru"'
  'si     "Sinhala" "si"'
  'sk     "Slovak" "sk"'
  'sl     "Slovenian" "sl"' 
  'sq     "Albanian" "sq"'
  'ta     "Tamil" "ta"'
  'tr     "Turkish" "tr"'
  'uk     "Ukrainian" "uk"'
  'vi     "Vietnamese" "vi"'
)

pkgname=()
_url=https://dev-builds.libreoffice.org/pre-releases/rpm

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=libreoffice-dev-${_locale,,}
  
  pkgname+=($_pkgname)

  source_x86_64+=("$_url/x86_64/${_pkgnamefmt}_${_LOver}_Linux_x86-64_rpm_${_pkgbase}_${_locale}.tar.gz")
  eval "package_$_pkgname() {
    _package $_lang
  }"
done

sha256sums_x86_64=('df8c8d989b566b93d882eebbd38faddc210a57a91202b1f4f4cdaec98e2dda76'
                   '25933130957af0bf1088d67c43dc9b0f475f58ab630e3196b82707c69e2df437'
                   '02174043d7720af8177c9742369956ef0d5dee4e999b85e59adb70ab38c50018'
                   'ba0a48dfca4e571e6e58016d925c9ccb23fbc900ec9c54f7b40a04d1497a2432'
                   '9d966708aa57fea6caf6a7bcafec559a05c8202ed744e5a793000dc52d8db477'
                   '2e9d9578be6fc8ae02b1b44e9a47daf9043ddddbe02b4422e0ee6f4d38ca6c8d'
                   '934fbaac10afa797ff179aee5bf95a8068c7a8e0b6caff8f96476a4d60714ece'
                   '070fa1a42436d40b9b87ae11c593ab6075a19da9a1510bdd092ebe584e8e1714'
                   'a74eca76c04c41439abe0704bfc76f0632ccc3b5d036c9d25d47c01eafa078c6'
                   '957046499b3b9cc549d30953b32e8767f7a85c5a72fe9bea4e92de35a4689a97'
                   '1c54df7425edd811ada9a1cbbf2a564d5dfccf0adb9f063c8cb98969b4b2c3a4'
                   '1431c4f487c404fcad19bd3eb9261608c9b772dc536506a2ca825f95364c0bdb'
                   'f13a01eb440673569e10940ab712b71e9902315002c35fc2ffcbd69ce7db0790'
                   'f698dd13739126f3191ed5f8309b54e4650c2822aa3985cc3aacf19a5dc66901'
                   'dbc888fd3a41d415cde0a6788a51ea22a62f7efd57833797226147930f75b0d0'
                   '4b555496d9de8f834c297dd516adad3c15feda5e1a39ab7d34c9eefb38c37352'
                   '88af83deb98fe93f4b8024f62f942165da12f521c530c880dca06100a4b99a00'
                   '1f7f063561561660bc0cf68dbe6d152b2cb3770e1803a1d03c291b0def0cef49'
                   '7f8de217c5edb1204393f47cb382a64396c550e3e9ddfdf376e8d37e09bb99cc'
                   '0c3e4d3eb77c626628635f8941da0434dd94b874a703fd64ca171459a07fcb22'
                   'b09fbff9682a7f7948888dfaf01c3f2d9d7193451dd84aa91a23509914279242'
                   'b2665e673e623be05c4bb67e3e4063b254ff177bb9bcfe239c5bb4ba00360bc8'
                   'f609c3cf9b85303e6bcb51b275d021f2fc35c6017aeb53801f4c7a453c38b95f'
                   '560e745427d6da99557843c42a6cb7cd5a0f9234696b96fa54b8a48cd018cb52'
                   '9dda2545698df89af2ccb96400759b6faeeeba8d542fb5d54bb6a7b7d71f2a66'
                   'f353ffae7abe98ae310d1c5ff1c038fe79f30855919fca1786eebf85addb3963'
                   '09a9f55ee5760b8862310f20c2381de593458ad1944f8888a142aaddd53ae64e'
                   '6646cfccdff2cd61a5e896d6571e1356eaad69e7033986175d003ad2fce88cbf'
                   'a4b6e0f67b24886bd54e8bf6e41ed5ca50a653a095705d56c7be3b6bc66e87a5'
                   '35f9dceb24ad27b0982d7b980277f543638c560853962b11eefb660182069576'
                   'e444e90a125dc7eb00f2105bcd7969f657f2366c37553be655a6d3a33a7322ae'
                   '5654320d541d28768b1b680e42ee415898390d632fb3e80873b0915fb4f739fe'
                   '82c6dd41820eec7d5556952873e4eb56b63f24e154c93c5363427204c1bdc5c4'
                   '8b3d7dc05d8e67e118747413ee7cd322e5a7974b7827b91ca2a2488bedb7696e'
                   'a89c441947dc6a770af454ded5baf2bf24153d69722f486095e0cea669641e1b'
                   'ea631f78156eb436175c08a0c436edf9e4f84bf7ea88044fc97cc38c11faa3a7'
                   '82b92516836181d5fe6b89c557cb7f38a8b5292b48ba895cafa804a9483e3caa'
                   'bb8cd3d160dfa18f5afd73cd64672731c5ae74a72237174e186ed232e4cab250'
                   '5ce17e7974be186851bca48b4b6156ab324f1d2994c57c75c99433b5fda59264'
                   '57ec603c42caa460db18b2ceb6a87b03dcaa522b8b7ac5a0f843dc9c028441ed'
                   'fbdf09cd2e08e6cd9e195bdb4aee00a6a9d58fb68299e2d354b6a43e2020e110'
                   '93b464eda38b91e9f35ffc2d63bc8d28587835734b7b6bc718b37e601400b62a')

_package() {
    pkgdesc="$2 language pack for LibreOffice development branch"
    __locale=$1
    depends=("libreoffice-dev>=${pkgver}")
    optdepends=("libreoffice-dev-help-$3")
    provides=("libreoffice-dev-$3=${pkgver}" "libreoffice-fresh-$3=${pkgver}")
    conflicts=("libreoffice-dev-$3")
    replaces=("libreoffice-dev-$3-bin")
    
    find "${srcdir}/${_pkgnamefmt}_${_LOver}"*${__locale}/RPMS/*rpm -exec bsdtar -x -f '{}' -C "${pkgdir}" \;

    rm -rf ${pkgdir}/opt/libreoffice${_basever}/share/extensions/dict-en
    rm -rf ${pkgdir}/opt/libreoffice${_basever}/share/wordbook
}
