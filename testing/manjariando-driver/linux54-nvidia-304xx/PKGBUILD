# Based on the file created for Arch Linux by:
# Maintainer : Thomas Baechler <thomas@archlinux.org>

# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Roland Singer <roland@manjaro.org>

_linuxprefix=linux54
_extramodules=extramodules-5.4-MANJARO
pkgname=$_linuxprefix-nvidia-304xx
_pkgname=nvidia
_pkgver=304.137
pkgver=304.137_5.4.108_1
pkgrel=1
pkgdesc="NVIDIA drivers for linux."
arch=('i686' 'x86_64')
url="http://www.nvidia.com/"
makedepends=("$_linuxprefix" "$_linuxprefix-headers" "nvidia-304xx-utils")
provides=("$_pkgname=$pkgver" "$_linuxprefix-nvidia-legacy")
replaces=("$_linuxprefix-nvidia-legacy")
groups=("$_linuxprefix-extramodules")
conflicts=("$_linuxprefix-nvidia" "$_linuxprefix-nvidia-340xx" "$_linuxprefix-nvidia-390xx" "$_linuxprefix-nvidia-418xx"
            "$_linuxprefix-nvidia-430xx" "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-440xx" "$_linuxprefix-nvidia-450xx"
            "$_linuxprefix-nvidia-455xx" "$_linuxprefix-nvidia-460xx")
license=('custom')
install=nvidia.install
options=(!strip)
source=('0001-disable-mtrr-4.3.patch' '0002-pud-offset-4.12.patch' '0003-nvidia-drm-pci-init-4.14.patch'
		'0004-timer-4.15.patch' '0005-usercopy-4.16.patch' '0006-do_gettimeofday-5.0.patch'
		'0007-subdirs-5.3.patch' '0008-on-each-cpu-5.3.patch')
#source_i686=("http://us.download.nvidia.com/XFree86/Linux-x86/${_pkgver}/NVIDIA-Linux-x86-${_pkgver}.run")
source_x86_64=("http://us.download.nvidia.com/XFree86/Linux-x86_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run")
md5sums=('e703f2eed605bd12c3665d0d64cf6198'
         '21a161409c9cdba66723f67e3d481b62'
         'cd96685ea2021d00e4bef5bff6ee836f'
         '7bd80b2123f87b24c5cf6ad1076c0eac'
         '0a9624e40abc976b5d775450fb5f3f89'
         '2b1e2bba05e47609f27a7fb6350f8758'
         '843484b2338446f89e7797ca2737fb4c'
         '2b456e376c0635bcde262d801449a712')
md5sums_x86_64=('485506ee6a7c54780488dacddf1d56b1')
#md5sums_i686=('133098e70581f6b81c481338cc20f100')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

#[[ "$CARCH" = "i686" ]] && _pkg="NVIDIA-Linux-x86-${_pkgver}"
[[ "$CARCH" = "x86_64" ]] && _pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

prepare() {
  cd "$srcdir"
  sh "$_pkg.run" --extract-only
  cd "${_pkg}"
  # patches here
  patch -p1 --no-backup-if-mismatch -i "$srcdir"/0001-disable-mtrr-4.3.patch
  patch -p1 --no-backup-if-mismatch -i "$srcdir"/0002-pud-offset-4.12.patch
  patch -p1 --no-backup-if-mismatch -i "$srcdir"/0003-nvidia-drm-pci-init-4.14.patch
  patch -p1 --no-backup-if-mismatch -i "$srcdir"/0004-timer-4.15.patch
  patch -p1 --no-backup-if-mismatch -i "$srcdir"/0005-usercopy-4.16.patch
  patch -p1 --no-backup-if-mismatch -i "$srcdir"/0006-do_gettimeofday-5.0.patch
  patch -p1 --no-backup-if-mismatch -i "$srcdir"/0007-subdirs-5.3.patch
  patch -p1 --no-backup-if-mismatch -i "$srcdir"/0008-on-each-cpu-5.3.patch
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

    cd "${_pkg}/kernel"
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
     depends=("${_linuxprefix}" "nvidia-304xx-utils")

    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
}
