# Maintainer: Geyslan G. Bem <geyslan@gmail.com>

pkgname=pje-office
pkgver=1.0.20
pkgrel=1
pkgdesc="PJeOffice is a software made available by CNJ for electronic signing PJe system's documents"
arch=('i686' 'x86_64')
url='http://www.cnj.jus.br/wiki/index.php/PJeOffice'
license=('custom')
install=${pkgname}.install
source=("https://metainfo.manjariando.com.br/${pkgname}/br.jus.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}"/PJeOffice{48,64,128}.png)
#source_i686=(ftp://ftp.cnj.jus.br/pje/programs/${pkgname}-${pkgver}/${pkgname}_${pkgver}_i386.deb)
source_x86_64=("${pkgname}_${pkgver}_amd64.deb::https://cnj-pje-programs.s3-sa-east-1.amazonaws.com/${pkgname}/${pkgname}_amd64.deb")
install=${pkgname}.install
md5sums=('14203e6e0695550e3768e677cb3dfe75'
         '07235cd0be7392d8ec9e15391bbd4de8'
         'b52faaa162aea334177a87ee477e6fc0'
         '8a1fbd42eb8c6c29d0bf060863c1618b')
md5sums_x86_64=('6667b6aafab4f6fea74e20e928c0944f')
#md5sums_i686=('0e715d1d45c07066f903ababa3e29a34')

_fix() {
    local _launcher

    # why waste space?
    rm -rf usr/share/${pkgname}/jre

    # new launcher using default installed java
    _launcher=usr/share/${pkgname}/pjeOffice.sh
    echo "#!/bin/bash" > ${_launcher}
    echo -e "# PJeOffice CLEAN script\n" >> ${_launcher}
    echo -e "echo \"Iniciando o PJeOffice!\"\n" >> ${_launcher}
    echo -e "cd \"/usr/share/${pkgname}\"\n" >> ${_launcher}
    echo -e "java -jar pjeOffice.jar" >> ${_launcher}

    # load icon from package path
    sed -i "s/^Icon=.*/Icon=\/usr\/share\/${pkgname}\/shortcuts\/icons\/PJeOffice.png/" usr/share/${pkgname}/shortcuts/${pkgname}.desktop
}

prepare() {
    tar xf data.tar.xz
    _fix
}

package() {
    depends=('java-runtime=8' 'bash')
    optdepends=( 'safenetauthenticationclient: Safenet Authentication Client'
                'safesignidentityclient: Smart card PKCS#11 provider and token manager'
                'sac-core: Safenet Authentication Client (core version)'
                'shodo: Assinador Digital'
                'serpro-signer: Web Signer application'
                'softplan-websigner: The Web Signer native application')

    cp -R usr/ ${pkgdir}
    mkdir -p ${pkgdir}/usr/bin
    mv ${pkgdir}/usr/share/pje-office/pjeOffice.sh ${pkgdir}/usr/bin/pjeOffice
    chmod 755 ${pkgdir}/usr/bin/pjeOffice
    mkdir -p ${pkgdir}/usr/share/applications
    mv ${pkgdir}/usr/share/pje-office/shortcuts/${pkgname}.desktop \
        ${pkgdir}/usr/share/applications/br.jus.${pkgname/-/_}.desktop

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/PJeOffice${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/PJeOffice.png"
    done

    # Appstream
    install -D -m644 "${srcdir}/br.jus.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/br.jus.${pkgname}.metainfo.xml"
}
