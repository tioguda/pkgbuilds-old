# Maintainer: Dimitris Kiziridis <ragouel at outlook dot com>
# Maintainer: Fred Lins <fredcox at gmail dot com>
# install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"

pkgname=crazydiskmark
pkgver=0.7.3
pkgrel=1
pkgdesc='Linux disk benchmark tool like CrystalDiskMark'
arch=('any')
url='https://github.com/fredcox/crazydiskmark'
license=('MIT')
makedepends=('python-setuptools' 'imagemagick')
source=("${pkgname}-${pkgver}.tar.gz::https://pypi.io/packages/source/c/${pkgname}/${pkgname}-${pkgver}.tar.gz")
sha256sums=(c30509ed2c7026863e9db19b88a83b9b5eab6f9e8834d2d2790b9f2ee2c1f9db)

build() {
  	cd "${pkgname}-${pkgver}"
  	python setup.py build
}

package() {
	depends=('python-pyqt5'
         'python-coloredlogs'
         'python-humanfriendly'
         'fio')
         
  	cd "${pkgname}-${pkgver}"
  	python setup.py install --root="$pkgdir" --optimize=1 --skip-build
  	mkdir -p "$pkgdir/usr/share/applications/"
  	install -Dm644 "${pkgname}"/"${pkgname}".desktop "$pkgdir/usr/share/applications/"

	sed -i 's/Icon=.*/Icon=crazydiskmark/' "${pkgdir}/usr/share/applications/${pkgname}.desktop"
    # Fix and install desktop icons
    for size in 22 24 32 48 64; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgname}/images/crazydiskmark_icon.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done

  	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
