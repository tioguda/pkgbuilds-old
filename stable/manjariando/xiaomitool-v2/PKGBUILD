# Mélanie Chauvel (ariasuni) <perso@hack-libre.org>

_pkgname=xiaomitool
pkgname=xiaomitool-v2
pkgver=20.7.28
pkgrel=1.1
pkgdesc='Modding of Xiaomi devices made easy for everyone'
arch=(any)
license=(custom)
url="https://www.xiaomitool.com/V2"
options=('!strip')
#install=${_pkgname}.install
source=("https://github.com/francescotescari/XMT/releases/download/v${pkgver}/XMT2_Linux_${pkgver}.run"
        "https://metainfo.manjariando.com.br/${pkgname}/${_pkgname}"-{48,64,128}.png
        "https://metainfo.manjariando.com.br/${pkgname}/com.${_pkgname}.metainfo.xml"
        "${_pkgname}"
        "${_pkgname}."{menu,xml})
sha256sums=('be7b235376c06d158c2485eebc22ba299ee784639bee52dc50e210cda206afc6'
            'b0c2506e1da98586ee1b3b74488e9ee7990a48d52f677e4e7ce7b53775518447'
            'e5dd481aa7f7063a733434685c636b9a4a106fef16c256d045ad4ebd427fa7ad'
            'fff6eeab301412a1c6e1c9fd7748d270620e371539ee4506b123f3609801ff28'
            '929fcc2f5745154c8a1a9a767845f4ee19673650dadc1e15c460b7e89974ebb1'
            '007bb4cce8e53fa429ec39b0da4291f9b320d411fd0b9998aa6a7610227200a4'
            '36e3936fab66f84b807feb42993e4ded0251f5dd89b66362d65a2aa595d10faa'
            '5a31644a58b9cd0e7566f2cfd1f82754946b60aaab579c367f6f627627cc72d7')

_xiaomitool_desktop="[Desktop Entry]
Version=1.0
Name=XiaoMiTool V2
Exec=/usr/bin/xiaomitool %U
StartupNotify=true
Terminal=false
Icon=xiaomitool
Type=Application
Categories=Development;Utility;"

build() {
  cd $srcdir
  echo -e "$_xiaomitool_desktop" | tee com.${_pkgname}.desktop
}

package() {
  cd $srcdir
  sh "XMT2_Linux_${pkgver}.run" --noexec --keep

  mkdir "${pkgdir}/opt"
  mv XiaoMiTool-V2 "${pkgdir}/opt"

  mkdir -p "${pkgdir}/usr/bin"
  install -m755 xiaomitool "${pkgdir}/usr/bin"

  install -Dm644 "${srcdir}/xiaomitool.xml" "${pkgdir}/usr/share/gnome-control-center/default-apps/xiaomitool.xml"
  install -Dm644 "${srcdir}/xiaomitool.menu" "${pkgdir}/usr/share/menu/xiaomitool.menu"

  # Appstream
  rm -rf "${pkgdir}/usr/share/appdata"
  install -Dm644 "${srcdir}/com.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${_pkgname}.metainfo.xml"
  install -Dm644 "${srcdir}/com.${_pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${_pkgname}.desktop"

  for i in 48 64 128; do
    install -Dm644 "${srcdir}/${_pkgname}-${i}.png" \
        "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
  done
}
