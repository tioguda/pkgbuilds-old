# Maintainer: tioguda <guda.flavio@gmail.com>

_pkgname=chkrootkit
pkgname=${_pkgname}-bin
#extra=+b10
_pkgver=0.54-1
pkgver=${_pkgver/-/.}
pkgrel=1
pkgdesc="Rootkit detector. The chkrootkit security scanner searches the local system for signs that it is infected with a 'rootkit'."
arch=('i686' 'x86_64')
url="http://www.chkrootkit.org"
license=('BSD-2-Clause')
backup=('etc/cron.daily/chkrootkit')
options=('!strip' '!emptydirs')
install=${pkgname}.install
source=("https://metainfo.manjariando.com.br/${_pkgname}/org.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${_pkgname}/${_pkgname}"-{48,64,128}.png)
source_i686=("http://cdn-fastly.deb.debian.org/debian/pool/main/c/${_pkgname}/${_pkgname}_${_pkgver}_i386.deb")
source_x86_64=("http://cdn-fastly.deb.debian.org/debian/pool/main/c/${_pkgname}/${_pkgname}_${_pkgver}_amd64.deb")
sha512sums=('da5633829608ffa4d680a94ea03f9d67b1feb554fd0112a20e3f17ef1c73184e8acde2c5065624d90489cce6b9b29506458977f80da3842c6c0b8eb13769121c'
            '385250f4077029f4e750f220efc34492414874517b5250c74820a36fcb2fdc2b33af330288019cffba6f423bd6b94bf840c79409de9da0c861e8a458fb0eac99'
            'f31f30619bc311bd94e597c9be000ba8faf097c0b3e77b825d15a2387cacd4e3b3bb5f4bb45051058fa6f1bd051bb146f57c2b3b10edb1a729043df849c37a39'
            '5d1e250ff9c9f02e9f3a0c25f16d6a7194e958a8b97e10b5b4efd311c0c1d74e302b0a64811bbaf45c332d1d29dc98b64cddb58bf619687b195a9fb02085cc47')
sha512sums_i686=('41fb9a4a6056dab29c000a3d44d87e89063bc8dd13f526ad72b5ffe69358ec3234824d102484d857c2575ff39d2558b3818f1bf7112accf233c6cafc65d7edbd')
sha512sums_x86_64=('d4730193cd855c26e6491b45e7ad0b81015aedada8c4a06a840e10ae3cde308a7be560c90c85a5da27fa12288dd84fc56cd24c69049a5d476bc33585ed2cd459')

_chkrootkit_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Security;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_chkrootkit_desktop" | tee org.${_pkgname}.desktop
}

package(){
    depends=('binutils' 'net-tools' 'openssh')

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    # Fix directories structure differencies
    cd "${pkgdir}"

    install -D -m644 "${pkgdir}/usr/share/doc/${_pkgname}/copyright" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    mkdir usr/bin 2> /dev/null; mv usr/sbin/* usr/bin; rm -rf usr/sbin

    # Appstream
    install -Dm644 "${srcdir}/org.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.${_pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/org.${_pkgname}.desktop" \
        "${pkgdir}/usr/share/applications/org.${_pkgname}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${_pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${_pkgname}.png"
    done
}
