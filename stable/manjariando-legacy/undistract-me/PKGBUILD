# Maintainer: ahrs <Forward dot to at hotmail dot co dot uk>

pkgname=undistract-me
pkgver=r79
pkgrel=1
arch=('any')
url="https://github.com/jml/undistract-me"
depends=('bash' 'libnotify' 'xorg-xprop')
conflicts=('undistract-me-git')
license=("MIT")
pkgdesc="Notifies you when long-running terminal commands complete"
install=${pkgname}.install
source=("git://github.com/jml/undistract-me.git"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}"-{48,64,128}.png
        'ignore-windows-check.patch')
sha256sums=('SKIP'
            '6c424e3ac89295ab2d68f31c77884de2dfc6cdaf595d691432e60fab0e8141c1'
            'c7969c4deb460cc9ecd57e7dc1910fc1f6044b12316befabe92796129d97aa84'
            '64eadc42b5c611254bd47569239d5ba1a5ce6c4391497f7f97c262009c0d2197'
            'e7b891cb89d20d64b537864dbcdc278096b44c51fb548252da81cdb4eea5be01'
            'e0710489eaf48490635097edf8bd8734dde43db7cee2cae4671cd4a45c50477f')

pkgver() {
    cd "$srcdir/undistract-me"
    printf "r%s" "$(git rev-list --count HEAD)"
}

prepare() {
    patch -p1 -i ./ignore-windows-check.patch
}

_undistract_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_undistract_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    mkdir -p "$pkgdir/usr/share/undistract-me"
    mkdir -p "$pkgdir/usr/share/doc/undistract-me"
    mkdir -p "$pkgdir/etc/profile.d/"
    sed -ni '/Copyright/,/developers\"./p' "$srcdir/undistract-me/LICENSE"
    install -D -m644 "$srcdir/undistract-me/long-running.bash" "$pkgdir/usr/share/undistract-me/long-running.bash"
    install -D -m644 "$srcdir/undistract-me/preexec.bash" "$pkgdir/usr/share/undistract-me/preexec.bash"
    install -D -m644 "$srcdir/undistract-me/undistract-me.sh" "$pkgdir/etc/profile.d/undistract-me.sh"
    install -D -m644 "$srcdir/undistract-me/README.md" "$pkgdir/usr/share/doc/undistract-me/README.md"
    install -D -m644 "$srcdir/undistract-me/LICENSE" "$pkgdir/usr/share/doc/undistract-me/COPYING"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
