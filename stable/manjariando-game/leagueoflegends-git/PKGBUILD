# Maintainer: Kuan-Yen Chou <kuanyenchou@gmail.com>
# Contributor: Timofey Titovets <nefelim4ag@gmail.com>

pkgname=leagueoflegends-git
pkgver=0.10.25.r10
pkgrel=1
pkgdesc="League of Legends helper script"
arch=('any')
url="https://github.com/kyechou/leagueoflegends"
license=('GPL3')
depends=('wine-lol' 'winetricks' 'bash' 'curl' 'openssl' 'lib32-gnutls'
        'lib32-libldap' 'lib32-openal' 'lib32-libpulse' 'vulkan-icd-loader'
        'lib32-vulkan-icd-loader' 'vulkan-driver' 'lib32-vulkan-driver')
makedepends=('git')
optdepends=("lib32-amdvlk: AMD Vulkan driver"
            "lib32-nvidia-utils: NVIDIA Vulkan driver"
            "lib32-vulkan-intel: Intel's Vulkan mesa driver"
            "lib32-vulkan-radeon: Radeon's Vulkan mesa driver"
            "zenity: Loading screen indication")
conflicts=("${pkgname%%-git}")
install=${pkgname%%-git}.install
source=("$pkgname"::'git+https://github.com/kyechou/leagueoflegends.git'
        "https://manjariando.gitlab.io/metainfo/${pkgname%%-git}/com.${pkgname%%-git}.metainfo.xml"
        "https://manjariando.gitlab.io/metainfo/${pkgname%%-git}/${pkgname%%-git}"-{48,64,128}.png)
md5sums=('SKIP'
         'f5523d3fcee76a3c35d79a9da75076a4'
         '817cc14d70577bf9611592f053147a12'
         '3b56ab0f15cd0a4414027e8589652f1a'
         'ee7aec835cfdee724aa3a83570c15a47')

pkgver() {
    cd "$srcdir/$pkgname"
    _pkgver=$(git describe --long --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g')
    printf '%s' "$( cut -f1-4 -d'.' <<< ${_pkgver} )"
}

package() {
    cd "$srcdir/$pkgname"
    make DESTDIR="${pkgdir}" install

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname%%-git}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname%%-git}.metainfo.xml"
    mv "${pkgdir}/usr/share/applications/${pkgname%%-git}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname%%-git}.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/${pkgname%%-git}-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname%%-git}.png"
    done
}
