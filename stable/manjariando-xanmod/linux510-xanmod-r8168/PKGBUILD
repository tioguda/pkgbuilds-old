# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Based on the file created for Arch Linux by:
# Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Bob Fanger <bfanger(at)gmail>
# Filip <fila pruda com>, Det <nimetonmaili(at)gmail>

_linuxprefix=linux510-xanmod
_extramodules=extramodules-5.10-xanmod-MANJARO
pkgname=$_linuxprefix-r8168
_pkgname=r8168
_pkgver=8.048.03
pkgver=8.048.03_5.10.27.xanmod1_1.1
pkgrel=1
pkgdesc="A kernel module for Realtek 8168 network cards"
url="http://www.realtek.com.tw"
license=("GPL")
arch=('x86_64')
depends=('glibc' "$_linuxprefix")
makedepends=("$_linuxprefix-headers")
provides=("$_pkgname=$_pkgver")
groups=("$_linuxprefix-extramodules")
source=("https://github.com/mtorromeo/r8168/archive/$_pkgver/$_pkgname-$_pkgver.tar.gz")
sha256sums=('0f191c4dc2f2e1aef6789d34820305fae27690d20212f2ac12ded634eb786d8c')

install=$_pkgname.install

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    cd "$_pkgname-$_pkgver"
}

build() {
    _kernver="$(cat /usr/lib/modules/$_extramodules/version || true)"

    cd "$_pkgname-$_pkgver"

    # avoid using the Makefile directly -- it doesn't understand
    # any kernel but the current.

    make -C /usr/lib/modules/$_kernver/build \
      M="$srcdir/$_pkgname-$_pkgver/src" \
      EXTRA_CFLAGS="-DCONFIG_R8168_NAPI -DCONFIG_R8168_VLAN -DCONFIG_ASPM -DENABLE_S5WOL -DENABLE_EEE" \
      modules
}

package() {
    cd "$srcdir/$_pkgname-$_pkgver/src"
    install -D -m644 $_pkgname.ko "$pkgdir/usr/lib/modules/$_extramodules/$_pkgname.ko"

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${_pkgname}.install"

    find "$pkgdir" -name '*.ko' -exec gzip -9 {} \;
}
