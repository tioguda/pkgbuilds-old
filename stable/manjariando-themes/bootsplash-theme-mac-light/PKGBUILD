# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: holoitsme <bisio.97@gmail.com> #https://github.com/holoitsme/manjaro-bootsplash-maker/commit/056460ed66302aa470b72947f036724bb7d196d1

pkgbase=mac-light
_pkgname=bootsplash-theme-${pkgbase%%-light}
pkgname=${_pkgname}-light
pkgver=20210222
pkgrel=1
url="https://manjariando.com.br/2020/11/13/bootsplash-mac-os"
arch=('any')
license=('GPL')
makedepends=('imagemagick')
install=bootsplash-theme.install
options=('!libtool' '!emptydirs')

source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/bootsplash-theme-manjariando-white/boot-white"-{48,64,128}.png
        'bootsplash-packer'
        'bootsplash-mac-light.sh'
        'bootsplash-mac-light.initcpio_install'
        'spinner.gif'
        'logo.png')

sha256sums=('537ef41a4ec604e335d8c525d2ba465cb4ddc3997d1cb840a43003e5d13b43a6'
            '1e8838f4322ca1ba7a6f2b522dfa188d39ba0020972a9ce181695d2d0d6a7a81'
            '2e2ca53057d98c25d3a5df830bf37c2f47327f748ad547bc85c53a43630b2937'
            '383e060247b24c2622a2547c9f092bf6e76d9a7943be1f51a153848c1830f85c'
            '51559d3ccfb448b03fa6439faf5869dbd0c2fbda1b5d5bf5d4ba70e60937472a'
            '1b5d02988e6073380f46df71bb7e4a119525ad79f2fbe36c1eefeb4e4f151cb5'
            '091f2ca4c364a0c219cf3991fd6566383df648ef7c22d4bc5b955a156a87a6ed'
            'af3fc14396a365fd2b11fe7a61d0cc14337573ef65b8451053334e298d021873'
            '335bddebcd4083be119b8a42a842bf3e055809848a7523284bb3c6cbc31cf44c')

pkgver() {
    _date=$(date +%F | sed s@-@@g)
    echo ${_date}
}

_bootsplash_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_bootsplash_desktop" | tee com.${_pkgname//-/_}.desktop
    sh ./bootsplash-mac-light.sh
}

package() {
    pkgdesc="Bootsplash Theme Mac OS logo"
    depends=('bootsplash-manager')

    cd "$srcdir"

    install -Dm644 "$srcdir/bootsplash-mac-light" "$pkgdir/usr/lib/firmware/bootsplash-themes/mac-light/bootsplash"
    install -Dm644 "$srcdir/bootsplash-mac-light.initcpio_install" "$pkgdir/usr/lib/initcpio/install/bootsplash-mac-light"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -d ${pkgdir}/usr/share/licenses/${pkgname}
    ln -s /usr/share/licenses/repo-manjariando/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${_pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${_pkgname//-/_}_light.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/boot-white-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:THEME=.*:THEME=${pkgbase}:" "${startdir}/bootsplash-theme.install"
} 
