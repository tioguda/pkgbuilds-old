# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: holoitsme <bisio.97@gmail.com> #https://github.com/holoitsme/manjaro-bootsplash-maker/commit/056460ed66302aa470b72947f036724bb7d196d1

pkgbase=manjariando-white
_pkgname=bootsplash-theme-${pkgbase%%-white}
pkgname=${_pkgname}-white
pkgver=20210222
pkgrel=1
url="https://manjariando.com.br/2020/06/16/bootsplash-e-splashscreen-do-blog-com-background-branco"
arch=('any')
license=('GPL')
makedepends=('imagemagick')
install=bootsplash-theme.install
options=('!libtool' '!emptydirs')

source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/boot-white"-{48,64,128}.png
        'bootsplash-packer'
        'bootsplash-packer.rst'
        'bootsplash-manjariando-white.sh'
        'bootsplash-manjariando-white.initcpio_install'
        'spinner.gif'
        'logo.png')

sha256sums=('31f996b38f785d912032cdeb0beba6d4b6a84bc962e75e6f3ef1c3cb06f20b0f'
            '1e8838f4322ca1ba7a6f2b522dfa188d39ba0020972a9ce181695d2d0d6a7a81'
            '2e2ca53057d98c25d3a5df830bf37c2f47327f748ad547bc85c53a43630b2937'
            '383e060247b24c2622a2547c9f092bf6e76d9a7943be1f51a153848c1830f85c'
            '51559d3ccfb448b03fa6439faf5869dbd0c2fbda1b5d5bf5d4ba70e60937472a'
            '35596e3f9c62d5f50730992befb940bfa75a24a2da15b558364300b52b64aa87'
            '634f462f04a28125adf02d5f034ec33b9b11c03cf5ae779e8c34333786d9f443'
            '453bfa69d711e37e858849d2ed3f5ed6d847a728ef8344531d2d61265102a61d'
            'd5ad514cf95f08f551829095b664c7add918ad3fc6a8195ff8c54c55605d7ff5'
            'c40e1ba2d2cd24be7539c2b3ecc41e4d056dcdd50dc254dbbe1dc716a0f3bede')

pkgver() {
  _date=$(date +%F | sed s@-@@g)
  echo ${_date}
}

_bootsplash_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_bootsplash_desktop" | tee com.${_pkgname//-/_}.desktop
    sh ./bootsplash-manjariando-white.sh
}

package() {
    pkgdesc="Bootsplash Theme 'Manjariando White'"
    depends=('bootsplash-manager')

    cd "${srcdir}"

    install -Dm644 "${srcdir}/bootsplash-manjariando-white" "${pkgdir}/usr/lib/firmware/bootsplash-themes/manjariando-white/bootsplash"
    install -Dm644 "${srcdir}/bootsplash-manjariando-white.initcpio_install" "${pkgdir}/usr/lib/initcpio/install/bootsplash-manjariando-white"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -d ${pkgdir}/usr/share/licenses/${pkgname}
    ln -s /usr/share/licenses/repo-manjariando/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${srcdir}/com.${_pkgname//-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${_pkgname//-/_}_white.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/boot-white-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done

    sed -i "s:THEME=.*:THEME=${pkgbase}:" "${startdir}/bootsplash-theme.install"
}
